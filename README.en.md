# Woolfelt DIY

#### Description

The small life of wool felt is a gathering place for wool felt enthusiasts, who will irregularly share their wool felt works. Welcome felt friends to contribute to the official account "羊毛毡DIY". Your works will have the opportunity to appear here

You can collect creative inspiration and save useful materials through him; Collect your favorite works and share them with friends with one click; Display your personal works to gain more exposure; Occasionally organize activities to select lucky users to present wool felt works or wool materials; More hidden skills, please look forward to
 
#### Demo - If you like it, Just Star it

Scan to Woolfelt DIY

![Woolfelt DIY](https://woolfeltdiy.snowinmay.fun/wp-content/uploads/woolfeltdiy-code.jpg) 

#### Thanks to

Thank you again for the open source version of the Weibo applet, written by Wang Tian using colorUI

