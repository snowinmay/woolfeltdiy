/*
 * 
 * 王天博客小程序开源版
 * 基于WordPress开源程序和WordPress REST API开发
 * organization: 王天博客  wx.dslcv.com
 * 码云地址：https://gitee.com/wangtianZS    
 * 技术支持微信号：ycygyzyq
 * 开源协议：MIT
 * Copyright (c) 2022  王天博客 https://wx.dslcv.com All rights reserved.
 * 
 */

var Api = require('../../utils/api.js');
var util = require('../../utils/util.js');
var wxApi = require('../../utils/wxApi.js')
var wxRequest = require('../../utils/wxRequest.js')
var Auth = require('../../utils/auth.js');
import config from '../../utils/config.js'
var app = getApp();

var webSiteName=config.getWebsiteName;
var domain =config.getDomain


Page({
  data: {
    title: '个人信息',
    pageData: {},
    pagesList: {},
    display: 'none',
   
    praiseList:[],
    dialog: {
        title: '',
        content: '',
        hidden: true
    },
    userInfo: {avatarUrl:"/images/gravatar.png",nickName:"",isLogin:false},
    avatarUrl:"/images/gravatar.png",
    nickName:"",
    isLoginPopup: false,
    openid:"",
    system:"",
    method:'补全资料',
    webSiteName:webSiteName,
    domain:domain,
    downloadFileDomain: config.getDownloadFileDomain,
    businessDomain:wx.getStorageSync('businessDomain'),
   
    
  },
  onLoad: function (options) {
    var self = this;    
    console.log(self.data)
    Auth.setUserInfoData(self);
    this.setData({
      avatarUrl:this.data.userInfo.avatarUrl,
      nickName:this.data.userInfo.nickName
    })
    if (self.data.openid) {
      self.setData({method:'更新信息'})
    }
    wx.getSystemInfo({
          success: function (t) {
          var system = t.system.indexOf('iOS') != -1 ? 'iOS' : 'Android';
          self.setData({ system: system });

        }
      })

    let pages = getCurrentPages();//当前页面    （pages就是获取的当前页面的JS里面所有pages的信息）
  },
  onUnload() {
      console.log("onUnload")
    },
  onChooseAvatar(e) {
    console.log(e.detail)
    const { avatarUrl } = e.detail 
    this.setData({
      avatarUrl
    })
    this.setData({
      "userInfo.avatarUrl":avatarUrl
    })
  },
  getNickname: function (e){
      console.log(e.detail.value)
      this.setData({
        "userInfo.nickName":e.detail.value
      })
  },
  updateInfo(appPage){
    var self = this;    
    let args = {}
    let userInfo = appPage.data.userInfo  || {}
    args.avatarUrl=userInfo.avatarUrl;
    args.openid = appPage.data.openid;
    args.nickname=userInfo.nickName;
      console.log(args)
      var url = Api.getUpdateUserInfo();  
      var postOpenidRequest = wxRequest.postRequest(url, args);
        //获取openid
        wx.hideLoading();  
        postOpenidRequest.then(response => {
            if (response.data.status == '200') {
                var userLevel = response.data.userLevel;
                console.log(userInfo)
                wx.setStorageSync('userInfo', userInfo);
                wx.setStorageSync('userLevel', userLevel);
                self.setData({
                    userInfo: userInfo
                });
                self.setData({
                    userLevel: userLevel
                });
                
                let pages = getCurrentPages();//当前页面    （pages就是获取的当前页面的JS里面所有pages的信息）
                let prevPage = pages[pages.length - 2];//上一页面（prevPage 就是获取的上一个页面的JS里面所有pages的信息）
                prevPage.setData({
                  isBack: true
                })
                wx.navigateBack({
                  delta: 1
                })
            }
            else {
                wx.showToast({
                    title: response.data.message,
                    icon: 'success',
                    duration: 900,
                    success: function () {}
                })
            }
        })
  },
  userInfoHandle(){
    let self = this;
    if (!self.data.userInfo.avatarUrl) {
        wx.showToast({
           title: '头像不能为空', //提示的内容
           duration: 1000, //持续的时间
           mask: true //显示透明蒙层 防止触摸穿透
        })
        return false
    }
    if (!self.data.userInfo.nickName) {
      wx.showToast({
           title: '昵称不能为空', //提示的内容
           duration: 1000, //持续的时间
           mask: true //显示透明蒙层 防止触摸穿透
        })
        return false
    }
    if (self.data.openid) {
      //update
      this.updateInfo(self)
      console.log(self.data.userInfo)
    }else{
      Auth.checkAgreeGetUser(app, self, '0').then(res=>{
        let userInfo = res.userInfo
        if (userInfo.isLogin) {

          let pages = getCurrentPages();//当前页面    （pages就是获取的当前页面的JS里面所有pages的信息）
          let prevPage = pages[pages.length - 2];//上一页面（prevPage 就是获取的上一个页面的JS里面所有pages的信息）
          prevPage.setData({
            isBack: true
          })
          wx.navigateBack({
            delta: 1
          })
        }
      });
    }
  }
})